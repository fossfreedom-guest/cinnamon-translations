# Korean translation for linuxmint
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the linuxmint package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: linuxmint\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2015-10-17 11:53+0100\n"
"PO-Revision-Date: 2015-10-20 19:49+0000\n"
"Last-Translator: Jung-Kyu Park <bagjunggyu@gmail.com>\n"
"Language-Team: Korean <ko@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2018-06-26 10:13+0000\n"
"X-Generator: Launchpad (build 18688)\n"

#: panels/bluetooth/cc-bluetooth-panel.c:355
msgid "Yes"
msgstr "예"

#: panels/bluetooth/cc-bluetooth-panel.c:355
msgid "No"
msgstr "아니오"

#: panels/bluetooth/cc-bluetooth-panel.c:469
msgid "Bluetooth is disabled"
msgstr "블루투스 꺼짐"

#: panels/bluetooth/cc-bluetooth-panel.c:474
msgid "Bluetooth is disabled by hardware switch"
msgstr "하드웨어 스위치에서 블루투스가 꺼져있음"

#: panels/bluetooth/cc-bluetooth-panel.c:478
msgid "No Bluetooth adapters found"
msgstr "블루투스 어댑터를 찾을 수 없음"

#: panels/bluetooth/cc-bluetooth-panel.c:599
#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:247
msgid "Visibility"
msgstr "보이기"

#: panels/bluetooth/cc-bluetooth-panel.c:603
#, c-format
msgid "Visibility of “%s”"
msgstr "“%s” 보이기"

#: panels/bluetooth/cc-bluetooth-panel.c:647
#, c-format
msgid "Remove '%s' from the list of devices?"
msgstr "'%s'을(를) 장치 목록에서 제거하시겠습니까?"

#: panels/bluetooth/cc-bluetooth-panel.c:649
msgid ""
"If you remove the device, you will have to set it up again before next use."
msgstr "장치를 제거하면 다음 사용하기 전 다시 설정해야 합니다."

#: panels/bluetooth/bluetooth.ui.h:1
msgid "Set Up New Device"
msgstr "새 장치 설정"

#: panels/bluetooth/bluetooth.ui.h:2
msgid "Remove Device"
msgstr "장치 제거"

#: panels/bluetooth/bluetooth.ui.h:3
#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:423
msgid "Connection"
msgstr "연결"

#: panels/bluetooth/bluetooth.ui.h:4
msgid "page 1"
msgstr "1쪽"

#: panels/bluetooth/bluetooth.ui.h:5
msgid "page 2"
msgstr "2쪽"

#: panels/bluetooth/bluetooth.ui.h:6
msgid "Paired"
msgstr "페어링함"

#: panels/bluetooth/bluetooth.ui.h:7
msgid "Type"
msgstr "형식"

#: panels/bluetooth/bluetooth.ui.h:8
msgid "Address"
msgstr "주소"

#: panels/bluetooth/bluetooth.ui.h:9
msgid "Mouse and Touchpad Settings"
msgstr "마우스 및 터치패드 설정"

#: panels/bluetooth/bluetooth.ui.h:10
#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:495
msgid "Sound Settings"
msgstr "소리 설정"

#: panels/bluetooth/bluetooth.ui.h:11
#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:487
msgid "Keyboard Settings"
msgstr "키보드 설정"

#: panels/bluetooth/bluetooth.ui.h:12
#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:464
msgid "Send Files..."
msgstr "파일 보내기..."

#: panels/bluetooth/bluetooth.ui.h:13
#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:469
msgid "Browse Files..."
msgstr "파일 보기..."

#: generate_additional_files.py:24
#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:37
#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:71
#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:111
#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:144
#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:219
#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:223
#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:477
msgid "Bluetooth"
msgstr "블루투스"

#: generate_additional_files.py:24
msgid "Configure Bluetooth settings"
msgstr "블루투스 설정"

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:72
#, c-format
msgid "Authorization request from %s"
msgstr "%s로부터의 승인 요청"

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:78
#, c-format
msgid "Device %s wants access to the service '%s'"
msgstr "장치 %s가 서비스 '%s'에 접근 승인을 원합니다"

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:80
msgid "Always grant access"
msgstr "항상 접근 허용"

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:81
msgid "Grant this time only"
msgstr "이번만 승인"

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:82
msgid "Reject"
msgstr "거절"

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:112
#, c-format
msgid "Pairing confirmation for %s"
msgstr "%s에 대한 페어링 확인"

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:118
#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:152
#, c-format
msgid "Device %s wants to pair with this computer"
msgstr "장치 %s가 이 컴퓨터와 페어링을 원합니다"

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:119
#, c-format
msgid "Please confirm whether the PIN '%s' matches the one on the device."
msgstr "PIN '%s' 가 장치에 있는 것과 일치하는지 확인해 주세요."

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:121
msgid "Matches"
msgstr "일치함"

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:122
msgid "Does not match"
msgstr "일치하지 않음"

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:145
#, c-format
msgid "Pairing request for %s"
msgstr "%s에 대한 페어링 요청"

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:153
msgid "Please enter the PIN mentioned on the device."
msgstr "장치에 나온 PIN 을 입력하세요."

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:169
msgid "OK"
msgstr "확인"

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:170
msgid "Cancel"
msgstr "취소"

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:261
msgid "Send Files to Device..."
msgstr "파일 보내기..."

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:262
msgid "Set up a New Device..."
msgstr "새로운 장치 설정하기..."

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:286
msgid "Bluetooth Settings"
msgstr "블루투스 설정"

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:325
msgid "hardware disabled"
msgstr "하드웨어 비활성화"

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:433
msgid "disconnecting..."
msgstr "연결 끊는 중..."

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:446
msgid "connecting..."
msgstr "연결 중..."

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:478
msgid "Error browsing device"
msgstr "장치 검색 실패"

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:479
#, c-format
msgid "The requested device cannot be browsed, error is '%s'"
msgstr "요청하신 장치는 검색되지 않습니다, '%s' 오류입니다"

#: files/usr/share/cinnamon/applets/bluetooth@cinnamon.org/applet.js:490
msgid "Mouse Settings"
msgstr "마우스 설정하기"
